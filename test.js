import Ceramic from '@ceramicnetwork/core'
import IPFS from 'ipfs-core'
import dagJose from 'dag-jose'
import { sha256 } from 'multiformats/hashes/sha2'
import legacy from 'multiformats/legacy'

const hasher = {}
hasher[sha256.code] = sha256
const dagJoseFormat = legacy(dagJose, {hashes: hasher})

const ipfs = await IPFS.create({ ipld: { formats: [dagJoseFormat] } })

const ceramic = await Ceramic.create(ipfs)
