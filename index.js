// const codecName = 'kneo'
// const codecNumber = 0x220869

// import table from 'multicodec/src/base-table.json';
// // @ts-ignore
// table.baseTable = {
//   ...table.baseTable,
//   [codecName]: codecNumber
// }

// console.log(table);

import Libp2p from 'libp2p';
import IPFS from 'ipfs-core';
import TCP from 'libp2p-tcp';
import MulticastDNS from 'libp2p-mdns';
import Bootstrap from 'libp2p-bootstrap';
// const KadDHT = require('libp2p-kad-dht')
import MPLEX from 'libp2p-mplex';
import { NOISE } from 'libp2p-noise';
import uint8ArrayConcat from 'uint8arrays/concat.js';
import uint8ArrayToString from 'uint8arrays/to-string.js';

// private
import { generate } from 'libp2p/src/pnet/index.js';
import Protector from 'libp2p/src/pnet/index.js';

// ipfs dag format
// import multihashing from 'multihashing-async';
// import multicodec from 'multicodec';
// import CID from 'cids';
// console.log(multicodec.getName(codecNumber))

// ceramiq
// import Ceramic from '@ceramicnetwork/core';
// import dagJose from 'dag-jose';
// import { sha256 } from 'multiformats/hashes/sha2';
// import legacy from 'multiformats/legacy';

// orbitdb
import Gossipsub from 'libp2p-gossipsub';
import OrbitDB from 'orbit-db';

/**
 * Options for the libp2p bundle
 * @typedef {Object} libp2pBundle~options
 * @property {PeerId} peerId - The PeerId of the IPFS node
 * @property {Object} config - The config of the IPFS node
 * @property {Object} options - The options given to the IPFS node
 */

/**
 * This is the bundle we will use to create our fully customized libp2p bundle.
 *
 * @param {libp2pBundle~options} opts The options to use when generating the libp2p node
 * @returns {Libp2p} Our new libp2p node
 */
const libp2pBundle = (opts) => {
  // Set convenience variables to clearly showcase some of the useful things that are available
  const peerId = opts.peerId
  console.log('my peerid : '+peerId);
  console.info(opts);

  // const bootstrapList = opts.config.Bootstrap
  const bootstrapList = ['/ip4/192.168.1.50/tcp/57839/p2p/QmSjjMvoEDmEtS8hYbaK79X7zWTMnPaS7PevKRXKWAfFxq'];

  console.log(bootstrapList);

  // const swarmKey = new Uint8Array(95)
  // generate(swarmKey)
  const swarmKey = new Uint8Array([
    47, 107, 101, 121,  47, 115, 119, 97, 114, 109,  47, 112,
   115, 107,  47,  49,  46,  48,  46, 48,  47,  10,  47,  98,
    97, 115, 101,  49,  54,  47,  10, 99,  56,  53,  57, 102,
   100,  52,  99, 101, 101,  97, 100, 98,  97,  48,  52,  56,
    49,  55,  50,  97, 100,  52,  99, 53,  52,  97,  55,  49,
   100,  55,  56,  51,  50,  52,  56, 99, 100,  97,  99, 102,
    56,  51,  53,  52,  52,  48,  99, 53,  50,  52,  51,  99,
    49,  50,  51,  99,  53,  99, 100, 51,  50,  50, 102
  ]);
  console.info(swarmKey);

  // Build and return our libp2p node
  // n.b. for full configuration options, see https://github.com/libp2p/js-libp2p/blob/master/doc/CONFIGURATION.md
  return new Libp2p({
    peerId,
    addresses: {
      listen: ['/ip4/0.0.0.0/tcp/0'],
    },
    // Lets limit the connection managers peers and have it check peer health less frequently
    connectionManager: {
      minPeers: 25,
      maxPeers: 100,
      pollInterval: 5000
    },
    modules: {
      transport: [
        TCP
      ],
      streamMuxer: [
        MPLEX
      ],
      connEncryption: [
        NOISE
      ],
      peerDiscovery: [
        MulticastDNS,
        Bootstrap
      ],
      pubsub: Gossipsub,
      // dht: KadDHT,
      connProtector: new Protector(swarmKey),
    },
    config: {
      peerDiscovery: {
        autoDial: true, // auto dial to peers we find when we have less peers than `connectionManager.minPeers`
        mdns: {
          interval: 10000,
          enabled: true
        },
        bootstrap: {
          interval: 30e3,
          enabled: true,
          list: bootstrapList
        }
      },
      // Turn on relay with hop active so we can connect to more peers
      relay: {
        enabled: true,
        hop: {
          enabled: true,
          active: true
        }
      },
      // dht: {
      //   enabled: true,
      //   kBucketSize: 20,
      //   randomWalk: {
      //     enabled: true,
      //     interval: 10e3, // This is set low intentionally, so more peers are discovered quickly. Higher intervals are recommended
      //     timeout: 2e3 // End the query quickly since we're running so frequently
      //   }
      // },
      pubsub: {
        enabled: true
      }
    },
    metrics: {
      enabled: true,
      computeThrottleMaxQueueSize: 1000,  // How many messages a stat will queue before processing
      computeThrottleTimeout: 2000,       // Time in milliseconds a stat will wait, after the last item was added, before processing
      movingAverageIntervals: [           // The moving averages that will be computed
        60 * 1000, // 1 minute
        5 * 60 * 1000, // 5 minutes
        15 * 60 * 1000 // 15 minutes
      ],
      maxOldPeersRetention: 50            // How many disconnected peers we will retain stats for
    }
  })
}

async function main () {

  // Now that we have our custom libp2p bundle, let's start up the ipfs node!

  // const format = {
  //   codec: codecName,
  //   defaultHashAlg: multicodec.SHA2_256,
  //   util: {
  //     serialize (data) {
  //       return Buffer.from(JSON.stringify(data))
  //     },
  //     deserialize (buf) {
  //       return JSON.parse(buf.toString('utf8'))
  //     },
  //     async cid (buf) {
  //       const multihash = await multihashing(buf, format.defaultHashAlg)
  //       console.info(multihash);
  //       return new CID(1, format.codec, multihash)
  //     }
  //   }
  // }

  const node = await IPFS.create({
    libp2p: libp2pBundle,
    // ipld: { formats: [format] },
  });


  // OR CERAMIC...
  // const hasher = {};
  // hasher[sha256.code] = sha256;
  // const format = legacy(dagJose, {hashes: hasher});
  // const node = await IPFS.create({
  //   libp2p: libp2pBundle,
  //   ipld: { formats: [format] },
  // });
  // const ceramic = await Ceramic.create(node);

  // orbitdb
  const orbitdb = await OrbitDB.createInstance(node);

  // Create / Open a database
  const db = await orbitdb.log("kneolog");
  await db.load();

  // Listen for updates from peers
  db.events.on("replicated", address => {
    console.log(db.iterator({ limit: -1 }).collect());
  });

  db.events.on('write', (address, entry, heads) => console.log('orbitdb write') );
  db.events.on('peer', (peer) => console.log(peer) );

  // write orbitdb
  setInterval(async () => {
    await db.add( 314 , { val: Math.random(), updated: new Date().getTime() });
  }, 5000);
  // read orbitdb
  db.events.on('ready', () => {
    const items = db.iterator().collect().map(e => e.payload.value)
    items.forEach(e => console.log(e.name))
    // "hello world"
  })


  // libp2p 
  node.libp2p.on('peer:discovery', (peerId) => {
    console.log('Peer Discovered : ' + peerId.toB58String());
  });

  node.libp2p.connectionManager.on('peer:connect', (connection) => {
    console.log('Peer connected : ' + connection.remotePeer.toB58String())
  })

  node.libp2p.connectionManager.on('peer:disconnect', (connection) => {
    console.log('peer disconnected : ' + connection.remotePeer.toB58String());
  });

  // Lets log out the number of peers we have every 2 seconds
  setInterval(async () => {
    try {
      const peers = await node.swarm.peers()
      console.log(`The node now has ${peers.length} peers.`)
      console.info(peers)
    } catch (err) {
      console.log('An error occurred trying to check our peers:', err)
    }
  }, 2000);

  // Log out the bandwidth stats every 4 seconds so we can see how our configuration is doing
  setInterval(async () => {
    try {
      const stats = await node.stats.bw()
      console.log(`\nBandwidth Stats: ${JSON.stringify(stats, null, 2)}\n`)
    } catch (err) {
      console.log('An error occurred trying to check our stats:', err)
    }
  }, 4000);

  // const myobj = { name: 'doc_1', id:1 };
  // const { cid } = await node.add(JSON.stringify(myobj));
  // console.log('CID1='+cid);
  // QmSytU4B7NhSbEKp8PZEA2y3FAVfBvNy4aHcGmbmovMN2w

  // const myobj = { name: 'doc_2', id:2 };
  // const { cid } = await node.add(JSON.stringify(myobj));
  // console.log('CID2='+cid);
  // QmdBHkYErQzqd9DZsqEhph75RshVL6PZkhoc3UAbjMtMME

  // const data = {
  //   hello: 'kneo world test'
  // }

  // const cid = await node.dag.put(data, { format: codecName, hashAlg: format.defaultHashAlg });

  // console.info(`Put ${JSON.stringify(data)} = CID(${cid})`)
  // bafyreieden2laaq5v7mddz6ahqzj4rroqqlo5cgxkq23jakcelhfrcsznq
  
  // const { value } = await node.dag.get('bafyreieden2laaq5v7mddz6ahqzj4rroqqlo5cgxkq23jakcelhfrcsznq')
  // console.info(`Get DAG = ${value}`)

  // try to get ipfs file
  // const cid = 'QmdBHkYErQzqd9DZsqEhph75RshVL6PZkhoc3UAbjMtMME'
  // for await (const file of node.get(cid)) {
  //   console.log(file.type, file.path)
  //   if (!file.content) continue;
  //   const content = []
  //   for await (const chunk of file.content) {
  //     content.push(chunk)
  //   }
  //   console.log(content)
  // }
  const chunks = []
  for await (const chunk of node.cat('QmdBHkYErQzqd9DZsqEhph75RshVL6PZkhoc3UAbjMtMME')) {
      chunks.push(chunk)
  };
  console.info(uint8ArrayToString(uint8ArrayConcat(chunks)));

}

main()
